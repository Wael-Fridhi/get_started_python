## Tuto Python 3  Django

Pour démarer :

> git clone git@bitbucket.org:Wael-Fridhi/get_started_python.git
> git fetch -all 
> git checkout develop



### Liens utiles  : 

Liste des tutoriels : 
 https://tunix.atlassian.net/wiki/spaces/COM/pages/934707224/Training+Python+3+Django

Git Flow et commandes git utiles:
https://tunix.atlassian.net/wiki/spaces/COM/pages/24084497/Installer+configurer+git+-+utilisation+Git+Flow

Installation Python, Virtualenv, Django et Postgres:
https://tunix.atlassian.net/wiki/spaces/COM/pages/45744129/Installer+django+postgres+et+virtual+env+sous+Windows
 
